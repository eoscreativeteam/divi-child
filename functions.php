<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); // Must load style.css
        wp_enqueue_style( 'child-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css' ); 
        wp_enqueue_script( 'child-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js' );
      }

add_filter( 'auto_update_plugin', '__return_true' );  // Enable all plugin updates
add_filter( 'auto_update_theme', '__return_true' );  // Enable all theme updates
add_filter( 'allow_minor_auto_core_updates', '__return_true' ); // Enable minor updates


/* Remove Contact Form 7 Links from dashboard menu items if not admin */
if (!(current_user_can('administrator'))) {
	function remove_wpcf7() {
	    remove_menu_page( 'wpcf7' ); 
	}

	add_action('admin_menu', 'remove_wpcf7');
     }

 /* Disable Gutenberg */

add_filter('use_block_editor_for_post', '__return_false');

?>
